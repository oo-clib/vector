#ifndef GENERIC_DYNAMIC_ARRAY
#define GENERIC_DYNAMIC_ARRAY

#include <stddef.h>

#define VEC_GROWTH_RATE 2
#define DEFAULT_VEC_ALLOCATION 64

#undef SELF
#define SELF struct Vec* self

#undef CSELF
#define CSELF const struct Vec* self

typedef unsigned char byte_t;

/* SHOULD THE PREDICATE GIVE ACCESS TO DATALEN?
 typedef bool (*predicate_t)(size_t dataLen, void* val); */
#if __STDC_VERSION__ >= 199901L || defined(LIB_VEC_COMPATIBILITY) /* Checks for C99 */
#include <stdbool.h>
typedef bool (*predicate_t)(void* val);
#else
typedef int (*predicate_t)(void* val);
#endif 

typedef struct Vec {
    /* Members */
    byte_t* _ptr;
    size_t _len;
    size_t _cap;
    const size_t _dataLen;

/* Lets your remove the methods even though it kinda defeats the point. */
#ifndef NO_VEC_METHODS
    /* Methods */
    /*
     * Returns the length of the Vec.
     */
    size_t (*len)(CSELF);

    /*
     * Copies the pointer element at the top of the Vec.
     * Note that it will not free the given pointer by itself, you must do it yourself.
     */
    void (*push)(SELF, const void* val);
    
    /*
     * Returns the indexed element without checking if it is out of bound.
     */
    void* (*get)(CSELF, const size_t idx);

    /*
     * Removes the top element of the Vec.
     * Be aware that it does not set it to 0 and will just let it be overwritten after.
     */
    void (*pop)(SELF);

    /*
     * Nulls out everything in the vector and put back its length to 0.
     */
    void (*clear)(SELF);

/* Some methods are considered unnecessary so you can remove them easily. */
#ifndef BASIC_VEC_METHODS
    /*
     * Returns the allocated capacity of the Vec.
     */
    size_t (*capacity)(CSELF);

    /*
     * Insert an element in the Vec to the given index.
     * Note that it will not free the given pointer by itself, you must do it yourself.
     */
    void (*insert)(SELF, const void* val, const size_t idx);

    /*
     * Returns the removed element from the Vec.
     * /!\ BE SURE TO FREE THE GIVEN ELEMENT /!\
     */
    void* (*remove)(SELF, const size_t idx);

    /*
     * Append a another Vec to this Vec.
     */
    void (*append)(SELF, const struct Vec* other);
    
    /*
     * Splits the Vec from the given index, including the indexed element.
     */
    struct Vec (*splitOff)(SELF, const size_t idx);

    /*
     * Splits the Vec from the given index, not including the indexed element.
     */
    struct Vec (*splitAt)(SELF, const size_t idx);
    
    /*
     * Moves all elements matching the predicate to the returned Vec.
     * The predicate is a function pointer which must return a boolean.
     */
    struct Vec (*filter)(SELF, predicate_t p);

    /*
     * Returns the indexed element or NULL if out of bound.
     */
    void* (*getSafe)(CSELF, const size_t idx);
#endif /* BASIC_VEC_METHODS */
#endif /* NO_VEC_METHODS */
} Vec;

/*
 * Creates a Vec with the specified capacity and dataLen.
 * The dataLen is used to specify the length of the element in bytes, use sizeof().
 * The capacity is the allocated memory, it will double everytime it needs to grow.
 */
Vec VecWithCapacity(const size_t dataLen, const size_t capacity);

/*
 * Creates a Vec with the specified dataLen. 
 * The dataLen is used to specify the length of the element in bytes, use sizeof().
 */
Vec newVec(const size_t dataLen);

/*
 * Frees the pointer inside of the Vec and nulls out everything else.
 */
void freeVec(Vec* vector);

#undef CSELF
#undef SELF
#endif /* GENERIC_DYNAMIC_ARRAY */