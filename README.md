# Quickview
This is a simple library containing a dynamic array under the name of `Vec` which provides an Object Oriented like interface.

- [Examples](#examples)
    - [Declaration](#declaration)
    - [Add/Remove](#addremove-elements)
- [Features](#features)
    - [Full](#all-methods)
    - [Basic](#basic-methods)
    - [None](#no-methods)
    - [<C99](#compatibility)
- [Todo](#todo)

# Examples
## Declaration
First you need to create and delete your `Vec` as following:
```c
// main.c
#include "vec.h"

int main() {
    Vec vector = newVec(sizeof(int));
    
    // ...

    freeVec(&vector);
    return 0;
}
```
The `Vec` made to be as generic as possible needs to get an indication of the bytes held by the type.
Do not try to declare a vector with `sizeof(char)` and input an `int`, it will go wrong for sure.

## Add/Remove elements
Pushing at the end and popping at the end is done like so:
```c
// ...
// pushes from an already declared variable
int number = 0;
vector.push(&vector, &number);

// pushes without declaring a variable
vector.push(&vector, (int[]){8});

// removes the last inserted element
vector.pop(&vector);
// ...
```

It is also possible to insert and remove an element y specifying the index:
```c
// ...
// 4 will then be put before 0, the content is [4,0]
vector.insert(&vector, 0, (int[]){4});

// added 5, the content is [4,0,5]
vector.push(&vector, (int[]){5});

// removes the 2nd element, the content then is [4,5]
vector.remove(&vector, 1);
// ...
```

# Features
As pointers take place in the memory, it is possible to remove them in order to make a lighter `Vec` if needed.
The default size of a vec is `4 * arch` octets where arch is the 8 for 64bit and 4 for 32bit etc...
Each function pointer, or method, add one arch.

## All methods
Here is the total list of methods present in the `Vec` datastructure.
Those are all enabled by default and can be disabled by categories depending on the need.
- `.len()`          *returns the number of contained elements*
- `.push()`         *adds an element at the end*
- `.get()`          *gets an element from its index*
- `.pop()`          *removes the element at the end*
- `.clear()`        *removes every element and goes back to 0*
- `.capacity()`     *gives the capacity allocated* 
- `.insert()`       *inserts an element at a specific index*
- `.remove()`       *removes an element at a specific index*
- `.splitOff()`     *splits from the element including it*
- `.splitAt()`      *splits at the element without including it*
- `.filter()`       *returns all elements matching a condition*

## Basic methods
You can only get some basic methods by declaring `#define BASIC_VEC_METHODS` before including `vec.h` in your code.
You will then get the following methods :
- `.len()`
- `.push()`
- `.get()`
- `.pop()`
- `.clear()`

## No methods
You can completely remove any method by declaring `#define NO_VEC_METHODS` before including `vec.h` in your code.
Though it kind of defeats the point of the library, it is possible. 

## Compatibility
Although I have not tested it, the library is supposed to be fully compatible with versions prior to C99.
It is also possible to enable it manually by declaring `#define LIB_VEC_COMPATIBILITY` before including `vec.h`.

# Todo
- [x] Add a feature to only get basic methods.
- [x] Add `.getSafe()` that returns `NULL` when out of bounds.
- [ ] Add a variadic function to easily add multiple values.
- [ ] Add `.clone()` or something to allocate a new `Vec`.
- [ ] Add `.reverse()` to reverse the content of the `Vec`.