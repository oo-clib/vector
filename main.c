#define LIB_VEC_COMPATIBILITY

#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include "vec.h"
#include <stdbool.h>

bool isPositive(void* val) {
    return *(int*)val >= 0;
}

int main(int argc, char* argv[]) {
    printf("%zd", sizeof(Vec));
    return 0;
}