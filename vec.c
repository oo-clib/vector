#include "vec.h"
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* Other functions */
void checkInsertionOverflow(Vec* v) {
    if (v->_len >= v->_cap) {
        v->_cap *= VEC_GROWTH_RATE;
        byte_t* newPtr = realloc(v->_ptr, v->_cap);
        v->_ptr = newPtr;
    }
}

#ifndef NO_VEC_METHODS
/* Methods declarations */
inline static size_t vecLen(const Vec* v);
static void vecPush(Vec* v, const void* val);
inline static void* vecGet(const Vec* v, const size_t idx);
static void vecPop(Vec* v);
static void vecClear(Vec* v);

#ifndef BASIC_VEC_METHODS
inline static size_t vecCapacity(const Vec* v);
static void vecInsert(Vec* v, const void* val, const size_t idx);
static void* vecRemove(Vec* v, const size_t idx);
static void vecAppend(Vec* dest, const Vec* src);
static Vec vecSplitOff(Vec* v, const size_t idx);
inline static Vec vecSplitAt(Vec* v, const size_t idx);
static Vec vecFilter(Vec* v, predicate_t p);
inline static void* vecGetSafe(const Vec* v, const size_t idx);
#endif /* BASIC_VEC_METHODS */
#endif /* NO_VEC_METHODS */

Vec VecWithCapacity(const size_t dataLen, const size_t capacity) {
    const size_t roundedAlloc = (capacity / dataLen) * dataLen;
    byte_t* ptr = malloc(roundedAlloc);
    
    Vec result = {
        /* Members */
        ._ptr       = ptr,
        ._len       = 0,
        ._cap       = roundedAlloc,
        ._dataLen   = dataLen,

#ifndef NO_VEC_METHODS
        /* Methods */
        .len        = vecLen,
        .push       = vecPush,
        .get        = vecGet,
        .pop        = vecPop,
        .clear      = vecClear,
#ifndef BASIC_VEC_METHODS
        .capacity   = vecCapacity,
        .insert     = vecInsert,
        .remove     = vecRemove,
        .append     = vecAppend,
        .splitOff   = vecSplitOff,
        .splitAt    = vecSplitAt,
        .filter     = vecFilter,
        .getSafe    = vecGetSafe,
#endif /* BASIC_VEC_METHODS */
#endif /* NO_VEC_METHODS */
    };
    
    return result;
}

Vec newVec(const size_t dataLen) {
    return VecWithCapacity(dataLen, DEFAULT_VEC_ALLOCATION);
}

void freeVec(Vec* vector) {
    free(vector->_ptr);
    const size_t membersSize = sizeof(byte_t*) + 3 * sizeof(size_t);
    memset(vector->_ptr, 0, membersSize);
}

#ifndef NO_VEC_METHODS
inline static size_t vecLen(const Vec* v) {
    return (v->_len / v->_dataLen);
}

static void vecPush(Vec* v, const void* val) {
    /* Prevents an overflow if the added data is outside the capacity of the vector */
    checkInsertionOverflow(v);

    memcpy(v->_ptr + v->_len, val, v->_dataLen);
    v->_len += v->_dataLen;
}

inline static void* vecGet(const Vec* v, const size_t idx) {
    return (v->_ptr + idx * v->_dataLen);
}

static void vecPop(Vec* v) {
    /* Does not actually delete everything, just lets the user write on top of data */
    v->_len -= v->_dataLen;
}

static void vecClear(Vec* v) {
    /* Nulls out everything in the vector */
    memset(v->_ptr, 0, v->_len);
    v->len = 0;
}

#ifndef BASIC_VEC_METHODS
inline static size_t vecCapacity(const Vec* v) {
    return (v->_cap / v->_dataLen);
}

static void vecInsert(Vec* v,const void* val, const size_t idx) {
    checkInsertionOverflow(v);

    byte_t* idxPtr = v->_ptr + idx * v->_dataLen;
    /* Copies all previous elements to the right */
    memcpy(idxPtr + v->_dataLen, idxPtr, (v->len(v) - idx) * v->_dataLen);
    /* Copies this element to its place */
    memcpy(idxPtr, val, v->_dataLen);
    v->_len += v->_dataLen;
}

/* Analogous to vecInsert */
static void* vecRemove(Vec* v, const size_t idx) {
    byte_t* idxPtr = v->_ptr + idx * v->_dataLen;
    
    /* WE MUST MALLOC THIS ELSEWHERE (or not ?) */
    void* data = malloc(v->_dataLen);
    memcpy(data, v->get(v, idx), v->_dataLen);

    memcpy(idxPtr, idxPtr + v->_dataLen, (v->len(v) - idx) * v->_dataLen);
    v->_len -= v->_dataLen;

    return data;
}

static void vecAppend(Vec* dest, const Vec* src) {
    while (dest->_len + src->_len > dest->_cap) {
        dest->_cap *= VEC_GROWTH_RATE;
        byte_t* newPtr = realloc(dest->_ptr, dest->_cap);
        dest->_ptr = newPtr;
    }

    memcpy(dest->_ptr + dest->_len, src->_ptr, src->_len);
    dest->_len += src->_len;
}

static Vec vecSplitOff(Vec* v, const size_t idx) {
    size_t newCapacity = v->_len - idx * v->_dataLen;
    Vec result = VecWithCapacity(v->_dataLen, newCapacity);
    byte_t* idxPtr = v->_ptr + idx * v->_dataLen;
    memcpy(result._ptr, idxPtr, result._cap);
    result._len = result._cap;
    v->_len = idx * v->_dataLen;

    return result;
}

inline static Vec vecSplitAt(Vec* v, const size_t idx) {
    return vecSplitOff(v, idx - 1);
}

static Vec vecFilter(Vec* v, predicate_t p) {
    Vec result = newVec(v->_dataLen);

    for (size_t i = 0; i < v->len(v); ++i)
        if (p(v->get(v, i))) {
            void* data = v->remove(v, i--);
            result.push(&result, data);
            /* Should we free the given content inside of .push() or here ? */
            free(data);
        }

    return result;
}

inline static void* vecGetSafe(const Vec* v, const size_t idx) {
    return (idx < v->len(v)) ? v->get(v, idx) : NULL;
}
#endif /* BASIC_VEC_METHODS */
#endif /* NO_VEC_METHODS */